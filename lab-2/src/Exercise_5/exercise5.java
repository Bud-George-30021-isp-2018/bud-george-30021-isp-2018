package Exercise_5;

import java.util.Scanner;

/**
 *
 * @author GeorgeBud
 */
public class exercise5 {

    public static void vec_bubble() {
        int vec[] = new int[100];

        Scanner n = new Scanner(System.in);
        System.out.print("Lungimea: ");
        int a = n.nextInt();

        for (int i = 0; i < a; i++) {
            System.out.print("Elementul " + i + ": ");
            vec[i] = n.nextInt();
        }

        for (int i = 0; i < a; i++) {
            System.out.print(vec[i] + ", ");
        }

        System.out.println();

        for (int i = 0; i < a; i++) {
            for (int j = 1; j < (a - i); j++) {
                int temp;
                if (vec[j - 1] > vec[j]) {
                    temp = vec[j - 1];
                    vec[j - 1] = vec[j];
                    vec[j] = temp;
                }

            }
        }

        for (int i = 0; i < a; i++) {
            System.out.print(vec[i] + ", ");
        }
    }

    public static void main(String args[]) {
        System.out.println("Exercise 5");
        vec_bubble();
    }
}
