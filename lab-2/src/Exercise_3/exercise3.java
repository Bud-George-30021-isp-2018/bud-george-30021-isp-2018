
package Exercise_3;

/**
 *
 * @author GeorgeBud
 */

import java.util.Scanner;

public class exercise3 {
    static boolean is_prime(int a){
        if(a==1 || a==0){
            return false;
        }
        
        if(a==2 || a==3){
            return true;
        }
            
        for(int i=2; i<=a/2; i++){
            if(a%i==0){
                return false;
            }
        }
        return true;
    }
    
    public static void parcurs(int a, int b){
        int k=0;
        for(int i=a; i<=b; i++){
            if(is_prime(i)){
                System.out.print(i+", ");
                k++;
            }
        }
        System.out.println("\n Numere gasite sunt: "+k);
    }
    
    
    public static void main(String args[]){
        System.out.println("Exercise 3");
        Scanner n = new Scanner(System.in);
            System.out.print("Introduceti primul numar:");
            int a = n.nextInt();
            System.out.print("Introduceti al doilea numar:");
            int b = n.nextInt();
        parcurs(a,b);
    }
            
}