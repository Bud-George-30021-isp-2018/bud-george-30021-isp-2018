package Exercise_6;

/**
 *
 * @author GeorgeBud
 */
import java.util.Scanner;

public class exercise6 {

    public static int fac_recur(int n) {
        if (n == 0) {
            return 1;
        }
        return n * fac_recur(n - 1);
    }

    public static int fac_iter(int n) {
        int p = 1;
        for (int i = 1; i <= n; i++) {
            p = p * i;
        }
        return p;
    }

    public static void main(String args[]) {
        System.out.println("Exercise 6");
        Scanner n = new Scanner(System.in);
        System.out.print("Introduceti numarul: ");
        int a = n.nextInt();
        System.out.println("Metoda Recursiva: " + fac_recur(a));
        System.out.println("Metoda Iterativa: " + fac_iter(a));

    }
}
