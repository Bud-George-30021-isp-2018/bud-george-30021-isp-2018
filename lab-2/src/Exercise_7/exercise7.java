package Exercise_7;

import java.util.*;

/**
 *
 * @author GeorgeBud
 */
public class exercise7 {
    public static void main(String args[]) {
        System.out.println("Exercise 7");
        Random r = new Random();
        int a = r.nextInt(100);
        
        Scanner n = new Scanner(System.in);
        
        int tries = 3;
        do {
            int x = n.nextInt();
            tries--;
            if (x > a)
                System.out.println("Wrong answer, your number it too high");
            else if (x < a)
                System.out.println("Wrong answer, your number it too low");
            else 
                break;
        } while (tries > 0);
        if (tries == 0)
            System.out.println("You lost.");
    }
}
